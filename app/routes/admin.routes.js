const controller = require("../controllers/admin.controller");
const { authJwt } = require("../middleware");

module.exports = function(app) {
  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  app.get(
    "/api/admin/inventory/list",
    [authJwt.verifyToken, authJwt.isAdmin],
    controller.inventoryList
  );
  app.get(
    "/api/admin/inventory/:id",
    [authJwt.verifyToken, authJwt.isAdmin],
    controller.inventoryItem
  );
  app.post(
    "/api/admin/inventory/save",
    [authJwt.verifyToken, authJwt.isAdmin],
    controller.inventorySave
  );
  app.post(
    "/api/admin/inventory/:id/update",
    [authJwt.verifyToken, authJwt.isAdmin],
    controller.inventoryUpdate
  );
  app.get(
    "/api/admin/inventory/:id/remove",
    [authJwt.verifyToken, authJwt.isAdmin],
    controller.inventoryRemove
  );
};
