const mongoose = require("mongoose");

const Inventory = mongoose.model(
  "Inventory",
  new mongoose.Schema({
    name: {
      type: String,
      required: true
    },
    price:{
      type: Number,
      required: true
    },
    quantity:{type:  Number, required: true},
    unit:{type:  String, required: true},
    description: String
  })
);

module.exports = Inventory;