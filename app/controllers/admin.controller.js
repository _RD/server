const db = require("../models");
const ObjectId = require('mongodb').ObjectId; 

const Inventory = db.inventory;

// Get inventory list
exports.inventoryList = (req, res) => {
  var page = parseInt(req.query.page) || 0; //for next page pass 1 here
  var limit = 10;
  var query = {};
  Inventory.find(query)
    .sort({ name: 'asc' })
    .skip(page * limit) //Notice here
    .limit(limit)
    .exec((err, doc) => {
      if (err) {
        return res.json(err);
      }
      Inventory.countDocuments(query).exec((count_error, count) => {
        if (err) {
          return res.json(count_error);
        }
        return res.json({
          total: count,
          page: page,
          pageSize: doc.length,
          inventory: doc
        });
      });
    });
};

// Fetch single inventory items
exports.inventoryItem = (req, res) => {
  var _id = new ObjectId(req.params.id);
  Inventory.findById({'_id': _id})
 .then(function(doc) {
        if(!doc)
            throw new Error('No record found.');
      res.status(200).send({'error': false, 'inventory':doc });    
  });
};

// Crate new inventory
exports.inventorySave = (req, res) => {
  var inventoryItem = new Inventory(req.body);

  inventoryItem.save(function(err, data) {
    if(err) {
      if (err.name === "ValidationError") {
        let errors = {};
  
        Object.keys(err.errors).forEach((key) => {
          errors[key] = err.errors[key].message;
        });
  
        return res.status(400).send(errors);
      }
      res.status(500).send("Something went wrong");
    }
    else {
      res.status(200).send({'error': false, 'message':"Inventory added successfully."});
    }
  });
};

// Update inventory
exports.inventoryUpdate = (req, res) => {
  Inventory.findByIdAndUpdate(req.params.id, req.body, { new: true })
  .then((inventory) => {
    if (!inventory) {
      return res.status(404).send({
        error: true,
        message: "No inventory found",
      });
    }
    res.status(200).send({error: false, message: 'Inventory updated successfully.'});
  })
  .catch((err) => {
    return res.status(404).send({
      error: true,
      message: "error while updating the inventory",
    });
  });
};

// Delete Inventory
exports.inventoryRemove = (req, res) => {
  Inventory.findByIdAndRemove(req.params.id)
    .then((inventory) => {
      if (!inventory) {
        return res.status(404).send({
          error: true,
          message: "Inventory not found ",
        });
      }
      res.status(200).send({'error':false, message: "Inventory deleted successfully!" });
    })
    .catch((err) => {
      return res.status(500).send({
        'error':true,
        message: "Could not delete inventory ",
      });
    });
};